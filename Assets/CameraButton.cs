﻿using UnityEngine;
using System.Collections;

public class CameraButton : MonoBehaviour {

    bool buttonPressed;
    

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (buttonPressed) {
            SetButtonFalse();

            GameObject.Find("Ghost").GetComponent<GhostMovement>().SendMessage("PictureTaken");
            GameObject.Find("artifact1").GetComponentInChildren<Artifact>().SendMessage("PictureTaken");
            GameObject.Find("artifact2").GetComponent<Artifact>().SendMessage("PictureTaken");
            GameObject.Find("artifact3").GetComponent<Artifact>().SendMessage("PictureTaken");

        }
	}

    public void PressCameraButton()
    {
        buttonPressed = true;
    }

    public void SetButtonFalse()
    {
        buttonPressed = false;

    }

    public bool IsButtonPressed()
    {
        return buttonPressed;
    }
}
