﻿using UnityEngine;
using System.Collections;

public class RaycastHitDetect : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        RaycastHit hit;
        Vector3 fwd = transform.forward;
        if (Physics.Raycast(transform.position, fwd, out hit))
            hit.transform.SendMessage("HitByRay");
    }
}
