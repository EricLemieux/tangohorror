﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuButtonScripts : MonoBehaviour {

    public bool skipMenu;

    public string mainMenuSceneName;
    public string otherButtonSceneName1;
    public string otherButtonSceneName2;
   

	// Use this for initialization
	void Start () {
        if (skipMenu)
        {
            SceneManager.LoadScene(mainMenuSceneName);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartGame()
    {
        SceneManager.LoadScene(mainMenuSceneName);
    }

    public void ButtonTwoFunction()
    {
        SceneManager.LoadScene(otherButtonSceneName1);
    }

    public void ButtonThreeFunction()
    {
        SceneManager.LoadScene(otherButtonSceneName2);
    }


}
