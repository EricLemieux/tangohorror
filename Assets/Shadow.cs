﻿using UnityEngine;
using System.Collections;

public class shadow : MonoBehaviour {

    bool rayHit;
    Vector3 moveVec;

    void HitByRay()
    {
        Debug.Log("I was hit by a ray");
        rayHit = true;

    }

    // Use this for initialization
    void Start () {
        rayHit = false;
        moveVec = new Vector3(0, 0, 1);

    }
	
	// Update is called once per frame
	void Update () {
        if (rayHit) {
            transform.Translate(moveVec);
        }
	}
}
