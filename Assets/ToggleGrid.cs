﻿using UnityEngine;
using System.Collections;

public class ToggleGrid : MonoBehaviour {

    // The grid that the player will see
    GameObject grid;


    void Start()
    {
        grid = GameObject.Find("RuleOfThirdsGrid");
        // Disabled by default
        grid.SetActive(false);
    }

    public void ToggleUIGrid()
    {
        if (grid.activeInHierarchy == true)
        {
            grid.SetActive(false);
        }
        else
            grid.SetActive(true);
    }

}
