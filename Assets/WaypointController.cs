﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaypointController : MonoBehaviour {

    GameObject[] waypoints;

	// Use this for initialization
	void Start () {
        waypoints = new GameObject[7] { GameObject.Find("Point 0"), GameObject.Find("Point 1"), GameObject.Find("Point 2"), GameObject.Find("Point 3"), GameObject.Find("Point 4"), GameObject.Find("Point 5"), GameObject.Find("Point 6") };
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public Vector3 GetPosition(int waypointIndex) {
        GameObject.Find("DebugText2").GetComponent<Text>().text = "ISI TWORKING";
        return waypoints[waypointIndex].GetComponent<Transform>().position;
    }

}
