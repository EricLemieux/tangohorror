﻿using UnityEngine;
using System.Collections;

public class ProximityTest : MonoBehaviour {

    GameObject player;
    Color color;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("PlayerCamera");
        color = GetComponent<Renderer>().material.color;
        color.a = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (Vector3.Distance(player.transform.position, transform.position) <= 8)
        {
            color.a = 0;
        }
        else {
            color.a = 1;
        }
        GetComponent<Renderer>().material.color = color;
    }
}
