﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Artifact : MonoBehaviour {

    bool picture;
    Vector3 rotate;
    bool active;
    bool rayHit;

    void PictureTaken() {
        GameObject.Find("DebugText").GetComponent<Text>().text = "ACTIVE";
        picture = true;
        //insert symbol actives
    }

    void HitByRay() {
        GameObject.Find("DebugText2").GetComponent<Text>().text = "ACTIVE";
        rayHit = true;
    }

	// Use this for initialization
	void Start () {
        picture = false;
        rayHit = false;
        active = false;
        rotate = new Vector3(1.5f, 1.5f, 1.5f);
	}
	
	// Update is called once per frame
	void Update () {
        if (rayHit && picture)
            active = true;
        if (active) {
            transform.Rotate(transform.rotation * rotate);
        }
        picture = false;
        rayHit = false;
	}
}
